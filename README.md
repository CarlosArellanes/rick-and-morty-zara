# Rick and Morty Zara

## Description

This project was made to aid the evaluation of the developer's skills and code cleanliness.
This is an app that displays a catalogue of characters from the show Rick and Morty. It uses the following tools:

- Retrofit for REST api calls
- Jetpack Compose for views and navigation
- Dagger Hilt for dependency injection
- MVVM architecture

## Installation

Use the Url from the Git URL in which this project is stored to import it to Android Studio as a Git project; select File > New > Project from Version Control
For more information about importing projects from version control, read IntelliJ’s information about Version control: https://www.jetbrains.com/help/idea/version-control-integration.html
